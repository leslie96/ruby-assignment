class User < ActiveRecord::Base
  validates :first_name , presence: true , length: {minimum: 3, maximum: 30}
  validates :last_name , presence: true , length: {minimum: 3, maximum: 50}
  validates :email , presence: true , length: {minimum: 10, maximum: 70}  
    
end
